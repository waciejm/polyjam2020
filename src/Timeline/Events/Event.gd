extends Node2D
class_name Event

var _time
var _type
var _valid: bool = true

func get_type() -> int:
	return _type

func get_time() -> int:
	return _time

func set_time(time: float):
	_time = time

func invalidate():
	_valid = false
	($Sprite as Sprite).modulate = Color(0.5, 0.5, 0.5, 1.0)

func is_valid() -> bool:
	return _valid
