extends Event

var _length: float = 0.0

func _ready():
	_type = Global.events.welder

func get_length() -> float:
	return _length

func set_length(ln: float):
	_length = ln
