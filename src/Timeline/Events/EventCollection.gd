extends Node2D

func _ready():
	for i in get_children():
		i.set_time((i.position.x * self.scale.x) / 4.0)
		if (i.get_type() == Global.events.welder
		or i.get_type() == Global.events.wrench):
			i.set_length((i.get_node("Line").points[1].x * self.scale.x) / 4.0)
		remove_child(i)
		Global.get_timeline().get_node("Events").add_child(i)
		i.set_owner(Global.get_timeline().get_node("Events"))
		i.scale = Vector2(1.0,1.0)
		if (i.get_type() == Global.events.welder
		or i.get_type() == Global.events.wrench):
			(i.get_node("Line") as Line2D).points[1].x = i.get_length() * Global.get_timeline().event_speed# * (1 / Global.get_beat())
