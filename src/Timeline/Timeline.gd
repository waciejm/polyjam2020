extends Node2D
class_name Timeline

export (float) var event_speed = 100.0

var _time: float = 0 # in beats
var _time_debug: float = 1
var _time_last: float = 0
var _time_real: float = 0
var _track_no: int = 0
var _last_end: float = 0.0

var _event_radius: float = event_speed / 300.0
var _event_repair: float = 10.0
var _fail_damage: float = 20.0


func _process(delta: float):
	if (Global.get_state() == Global.game_states.GAMEPLAY):
		_time_real += delta
		_time += delta / Global.get_beat()
		_hit_shit(delta)
		_move_events()
		if (Global.get_sound().has_node("Track" + str(_track_no))):
			if (_time - _last_end > Global.get_sound().get_node("Track" + str(_track_no)).get_length()):
				next_track()


func get_time() -> float:
	return _time

func next_track():
	Global.get_table().pop()
	Global.get_queue().pop()
	if (Global.get_sound().has_node("Track" + str(_track_no))):
		Global.get_sound().get_node("Track" + str(_track_no)).stop()
		_time = Global.get_sound().get_node("Track" + str(_track_no)).get_length() + _last_end
		_last_end = Global.get_sound().get_node("Track" + str(_track_no)).get_length() + _last_end
		
	#_time = _last_end
	_track_no += 1
	if (Global.get_sound().has_node("Track" + str(_track_no))):
		var track = Global.get_sound().get_node("Track" + str(_track_no))
		Global.set_bpm(track.get_bpm())
		track.play()
	else:
		_end()

func _end():
	Global.set_state(Global.game_states.ENDING)

func _move_events():
	for i in $Events.get_children():
		i.position.x = (i.get_time() - _time) * event_speed

func get_nearest_event() -> Event:
	var dist: float = 1.0
	var nearest: Event = null
	for i in $Events.get_children():
		if (i.is_valid()):
			if (i.get_type() == Global.events.hammer
			or i.get_type() == Global.events.stapler):
				if (abs(i.get_time() - _time) < dist):
					dist = abs(i.get_time() - _time)
					nearest = i
			else:
				if (abs(i.get_time() - _time) < dist):
					dist = abs(i.get_time() - _time)
					nearest = i
				elif (abs(i.get_time() + i.get_length() - _time) < dist):
					dist = abs(i.get_time() + i.get_length() - _time)
					nearest = i
	return nearest

func _hit_shit(delta: float):
	if (Input.is_action_pressed("debug_v")):
		Global.get_table().repair(100 * delta)
	var target = get_nearest_event()
	var distance: float
	if (is_instance_valid(target)):
		distance = target.get_time() - _time
		if (target.get_type() == Global.events.welder
		or target.get_type() == Global.events.wrench):
			if (distance + target.get_length() + _event_radius < 0):
				target.invalidate()
		elif (target.get_type() == Global.events.hammer
		or target.get_type() == Global.events.stapler):
			if (distance + _event_radius < 0):
				target.invalidate()
	if (Global.get_hammer().get_just_used()):
		if (is_instance_valid(target)
		and abs(distance) < _event_radius
		and target.get_type() == Global.events.hammer):
			target.invalidate()
			Global.get_table().repair((1 - (distance / _event_radius)) * _event_repair)
		else:
			Global.get_table().damage(_fail_damage)
	if (Global.get_stapler().get_just_used()):
		if (is_instance_valid(target)
		and abs(distance) < _event_radius
		and target.get_type() == Global.events.stapler):
			target.invalidate()
			Global.get_table().repair((1 - (distance / _event_radius)) * _event_repair)
		else:
			Global.get_table().damage(_fail_damage)
	if (Global.get_welder().get_used()):
		if (is_instance_valid(target)
		and target.get_type() == Global.events.welder
		and (distance - _event_radius) < 0
		and (distance + target.get_length() + _event_radius) > 0):
			Global.get_table().repair(
				_event_repair * (delta / Global.get_beat()) / target.get_length())
		else:
			Global.get_table().damage(
				_fail_damage * (delta / Global.get_beat()))
	if (Global.get_wrench().get_used()):
		if (is_instance_valid(target)
		and target.get_type() == Global.events.wrench
		and (distance - _event_radius) < 0
		and (distance + target.get_length() + _event_radius) > 0):
			Global.get_table().repair(
				_event_repair * (delta / Global.get_beat()) / target.get_length())
		else:
			Global.get_table().damage(
				_fail_damage * (delta / Global.get_beat()))
