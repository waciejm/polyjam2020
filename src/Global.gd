extends Node

onready var _menu: Control = get_node("/root/Main/Menu")
onready var _gameplay: Node2D = get_node("/root/Main/Gameplay")
onready var _game_over: Node2D = get_node("/root/Main/GameOver")
onready var _ending: Node2D = get_node("/root/Main/Ending")
onready var _queue: Node2D = _gameplay.get_node("Queue")
onready var _table: Node2D = _gameplay.get_node("Table")
onready var _output: Node2D = _gameplay.get_node("Output")
onready var _tools: Node2D = _gameplay.get_node("Tools")
onready var _gtv: Node2D = _gameplay.get_node("GTV")
onready var _sound: Node2D = _gameplay.get_node("Sound")
onready var _timeline: Node2D = _gameplay.get_node("Timeline")
onready var _satisfaction: Node2D = _gameplay.get_node("Satisfaction")
onready var _events: Node2D = _timeline.get_node("Events")
onready var _hammer: Node2D = _tools.get_node("Hammer")
onready var _welder: Node2D = _tools.get_node("Welder")
onready var _wrench: Node2D = _tools.get_node("Wrench")
onready var _stapler: Node2D = _tools.get_node("Stapler")

enum game_states { MENU, GAMEPLAY, GAMEOVER, ENDING }
var game_state = game_states.MENU

enum events { hammer, welder, stapler, wrench }

var _running: float = false
var _beat: float = 60.0 / 120.0 # minute / bpm


func _process(_delta: float) -> void:
	if (Input.is_key_pressed(KEY_ESCAPE)):
		get_tree().quit()
	if (Input.is_action_just_pressed("debug_n")):
		get_timeline().next_track()


func get_gameplay() -> Node2D:
	return _gameplay

func get_game_over() -> Node2D:
	return _game_over

func get_ending() -> Node2D:
	return _ending

func get_satisfaction() -> Node2D:
	return _satisfaction

func get_queue() -> Node2D:
	return _queue

func get_table() -> Node2D:
	return _table

func get_output() -> Node2D:
	return _output

func get_gtv() -> Node2D:
	return _gtv

func get_menu() -> Control:
	return _menu

func get_tools() -> Node2D:
	return _tools

func get_sound() -> Node2D:
	return _sound

func get_timeline() -> Node2D:
	return _timeline

func get_events() -> Node2D:
	return _events

func get_hammer() -> Node2D:
	return _hammer

func get_welder() -> Node2D:
	return _welder

func get_wrench() -> Node2D:
	return _wrench

func get_stapler() -> Node2D:
	return _stapler


func get_beat() -> float:
	return _beat

func set_beat(spd: float) -> void:
	_beat = spd
	get_gtv().get_node("Gundolf").update_speed(_beat)

func set_bpm(bpm: float) -> void:
	_beat = 60 / bpm
	get_gtv().get_node("Gundolf").update_speed(_beat)


func get_state() -> int:
	return game_state

func set_state(state) -> void:
	if (state != game_state):
		# TURN OFF
		if (game_state == game_states.MENU):
			get_menu().hide()
		elif (game_state == game_states.GAMEPLAY):
			get_gameplay().hide()
		# TURN ON
		game_state = state
		if (state == game_states.MENU):
			get_menu().show()
		elif (state == game_states.GAMEPLAY):
			get_gameplay().show()
		elif (state == game_states.GAMEOVER):
			get_game_over().show()
		elif (state == game_states.ENDING):
			get_ending().show()
