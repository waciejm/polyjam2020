extends YSort
class_name Queue


func pop():
	var children = get_children()
	if (children.size() > 1):
		var i = children.size() - 1
		while (i >  0):
			children[i].position = children[i-1].position
			i -= 1
	if (children.size() > 0):
		var child = children[0]
		remove_child(child)
		Global.get_table().push(child)
