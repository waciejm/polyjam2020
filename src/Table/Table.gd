extends Node2D

var _item: Item = null
var _box: Box = null

func _process(delta: float):
	if (_item != null):
		$HP.text = str(_item.health)
	else:
		$HP.text = str(-1)

func push(box: Box):
	_item = box.get_node("Item")
	box.remove_child(_item)
	$ItemPoint.add_child(_item)
	_item.scale = Vector2(1.0,1.0)
	_item.position = Vector2(1.0,1.0)
	_item.show()

func damage(dmg: float):
	if (_item != null):
		_item.get_damaged(dmg)

func repair(hp: float):
	if (_item != null):
		_item.get_repaired(hp)

func pop():
	if (_item != null):
		if (_item is GTVItem):
			Global.get_gameplay().get_node("GTV").show()
		$ItemPoint.remove_child(_item)
		Global.get_output().push(_item)
		_item = null
