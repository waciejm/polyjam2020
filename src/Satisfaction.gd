extends Node2D

var satisfaction: float = 50.0

func change(sat: float):
	satisfaction += sat

func _process(_delta: float):
	_display()
	if (Input.is_action_pressed("debug_m")):
		change(_delta * 10)
	if (Input.is_action_pressed("debug_b")):
		change(_delta * 10 * -1)
	if (satisfaction <= 0):
		Global.set_state(Global.game_states.GAMEOVER)

func _display():
	$Bar.rect_size.x = (satisfaction / 100.0) * $BG.rect_size.x
	if (satisfaction < 50.0):
		$Bar.color = Color(1.0, satisfaction / 50.0, 0.0, 1.0)
	else:
		$Bar.color = Color(1 - ((satisfaction - 50.0) / 50.0), 1.0, 0.0, 1.0)
