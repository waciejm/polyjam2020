extends AnimatedSprite

func update_speed(beat: float):
	set_speed_scale((1.0 / beat) * frames.get_frame_count("default"))
