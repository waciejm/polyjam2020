extends AudioStreamPlayer
class_name Track

export (float) var _bpm: float = 120
export (float) var _length: float = 34

func finished():
	Global.get_timeline().next_track()

func get_bpm() -> float:
	return _bpm

func get_length() -> float:
	return _length
