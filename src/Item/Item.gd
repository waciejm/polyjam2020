extends Node2D
class_name Item

var health: float = 50.0

func get_damaged(dmg: float) -> void:
	health -= dmg
	if (health < 0.0):
		health = 0.0
	_update()

func get_repaired(hp: float) -> void:
	health += hp
	if (health >= 100.0):
		health = 100.0
	_update()

func _update() -> void:
	if (health >= 100):
		$Repaired.show()
		$Bad.hide()
		$Destroyed.hide()
	elif (health <= 0):
		$Destroyed.show()
		$Bad.hide()
		$Repaired.hide()
	else:
		$Bad.show()
		$Destroyed.hide()
		$Repaired.hide()
