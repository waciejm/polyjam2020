extends Item
class_name GTVItem

func _update() -> void:
	if (health >= 100):
		$Repaired.show()
		$Bad.hide()
	else:
		$Bad.show()
		$Repaired.hide()
