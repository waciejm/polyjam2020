extends Node2D

onready var _sprite: Sprite = get_node("Sprite")

export (float) var duration: float = 0.2

var time: float = 0.0
var running: bool = false
var current_particle: int = 0

func _process(delta: float) -> void:
	if (Global.get_state() == Global.game_states.GAMEPLAY):
		if (running):
			time += delta
			if (time >= duration):
				running = false
				_sprite.hide()
		if (Global.get_hammer().get_just_used()):
			running = true
			time = 0.0
			_sprite.show()
			_emit_particle()

func _emit_particle() -> void:
	get_node("Particles" + str(current_particle)).emitting = true
	current_particle += 1
	if (current_particle >= get_children().size() - 1):
		current_particle = 0
