extends Node2D
class_name Tool

var just_used: bool = false
var used: bool = false
export (bool) var looped: bool = false

export (String) var button_name: String

func _process(_delta: float) -> void:
	if (Global.get_state() == Global.game_states.GAMEPLAY):
		if (Input.is_action_pressed(button_name)):
			get_node("Pressed").show()
			get_node("NotPressed").hide()
			if (not used):
				just_used = true
				$Sound.play()
			else:
				just_used = false
			used = true
		else:
			if (looped):
				$Sound.stop()
			just_used = false
			used = false
			get_node("Pressed").hide()
			get_node("NotPressed").show()

func get_used() -> bool:
	return used

func get_just_used() -> bool:
	return just_used
