extends Node2D

func _process(delta: float) -> void:
	if (Global.get_state() == Global.game_states.GAMEPLAY):
		if (Global.get_wrench().get_used()):
			$Sprite.show()
			rotation -= 8 * PI * delta
			$Particles.emitting = true
		else:
			$Particles.emitting = false
			$Sprite.hide()
