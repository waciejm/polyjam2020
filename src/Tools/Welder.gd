extends Node2D

onready var _sprite: Sprite = $Sprite
onready var _smoke_particles: Particles2D = $SmokeParticles
onready var _flame_particles: Particles2D = $FlameParticles

func _process(_delta: float) -> void:
	if (Global.get_state() == Global.game_states.GAMEPLAY):
		if (Global.get_welder().get_used()):
			_sprite.show()
			_smoke_particles.emitting = true
			_flame_particles.emitting = true
		else:
			_sprite.hide()
			_smoke_particles.emitting = false
			_flame_particles.emitting = false
