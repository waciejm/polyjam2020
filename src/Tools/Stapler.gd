extends Node2D

onready var _sprite: AnimatedSprite = $Sprite

var current_particle: int = 0

func _process(_delta: float) -> void:
	if (Global.get_state() == Global.game_states.GAMEPLAY):
		if (Global.get_stapler().get_just_used()):
			_sprite.frame = 0
			_emit_particle()

func _emit_particle() -> void:
	get_node("Particles" + str(current_particle)).emitting = true
	current_particle += 1
	if (current_particle >= get_children().size() - 1):
		current_particle = 0
