extends Node2D

func push(item: Item):
	Global.get_satisfaction().change((item.health - 75.0) / 2.5)
	item.queue_free()
